var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var BicicletaSchema = new Schema({
  code: Number,
  color: String,
  modelo: String,
  ubicacion: {
    type: [Number],
    index: {
      type: '2dsphere',
      sparse: true
    }
  }
})

BicicletaSchema.statics.createInstance = function (code, color, modelo, ubicacion) {
  return new this({
    code,
    color,
    modelo,
    ubicacion
  })
}

BicicletaSchema.methods.toString = function () {
  return `Code: ${this.code} | Color: ${this.color} | Modelo: ${this.modelo} |`;
};

BicicletaSchema.statics.allBicis = function (cb) {
  return this.find({}, cb)
}

BicicletaSchema.statics.add = function (aBici, cb) {
  this.create(aBici, cb)
}

BicicletaSchema.statics.findByCode = function (code, cb) {
  this.findOne({ code }, cb);
}

BicicletaSchema.statics.deleteByCode = function (code, cb) {
  this.deleteOne({ code }, cb);
}

module.exports = mongoose.model('Bicicleta', BicicletaSchema)

/*
var Bicicleta = function (id, color, modelo, ubicacion) {
  this.id = id;
  this.color = color;
  this.modelo = modelo;
  this.ubicacion = ubicacion;
};

Bicicleta.prototype.toString = function () {
  return `ID: ${this.id} | Color: ${this.color} | Modelo: ${this.modelo} |`;
};

Bicicleta.allBicis = [];
Bicicleta.add = function (bici) {
  Bicicleta.allBicis.push(bici);
};

Bicicleta.findById = function (id) {
  var bici = Bicicleta.allBicis.find((b) => b.id == id)
  if (bici) return bici
  else throw new Error(`Bici con id: ${id} no encontrada`)
}

Bicicleta.removeById = function (id) {
  for (let i = 0; i < Bicicleta.allBicis.length; i++) {
    if (Bicicleta.allBicis[i].id == id) {
      Bicicleta.allBicis.splice(i, 1);
      break;
    }

  }
}

// var a = new Bicicleta(1, "verde", "montaña", [-34.599015, -58.380632]);
// var b = new Bicicleta(2, "verde", "montaña", [-34.607429, -58.383431]);

// Bicicleta.add(a);
// Bicicleta.add(b);

module.exports = Bicicleta
*/