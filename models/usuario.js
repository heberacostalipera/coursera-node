var mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const crypto = require("crypto");
var uniqueValidator = require("mongoose-unique-validator");
var Reserva = require("./reserva");
var Schema = mongoose.Schema;

const Token = require("../models/token");
const mailer = require("../mailer/mailer");

const saltRounds = 10;

function validateEmail(email) {
  const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,5})+$/;
  return re.test(email);
}

var usuarioSchema = new Schema({
  nombre: {
    type: String,
    trim: true,
    required: [true, "El nombre es obligatorio"]
  },
  email: {
    type: String,
    trim: true,
    required: [true, "El mail es obligatorio"],
    lowercase: true,
    unique: true,
    validate: [validateEmail, "Por favor ingrese un email valido"],
    match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
  },
  password: {
    type: String,
    required: [true, "El password es obligatorio"]
  },
  passwordResetToken: String,
  passwordResetTokenExpires: Date,
  verificado: {
    type: Boolean,
    default: false
  },
  googleId: String,
  facebookId: String
});

usuarioSchema.plugin(uniqueValidator, {
  message: "El {PATH} ya existe con otro usuario."
});

usuarioSchema.pre("save", function (next) {
  if (this.isModified("password")) {
    this.password = bcrypt.hashSync(this.password, saltRounds);
  }
  next();
});

usuarioSchema.methods.validPassword = function (password) {
  return bcrypt.compareSync(password, this.password);
};

usuarioSchema.methods.resetPassword = function (cb) {
  const token = new Token({
    _userId: this.id,
    token: crypto.randomBytes(16).toString("hex")
  });

  const emailDestination = this.email;

  token.save(function (err) {
    if (err) return cb(err);

    const mailOptions = {
      from: "no-reply@redbicicletas.com",
      to: emailDestination,
      subject: "Reseteo de Password de Cuenta",
      text:
        "Hola,?\n\n" +
        "Por favor, para resetear el password de su cuenta haga click en este link: \n" +
        "http://localhost:3000" +
        "/resetPassword/" +
        token.token +
        ".\n"
    };

    mailer.sendMail(mailOptions, function (err) {
      if (err) return cb(err);

      console.log(
        `Se envio un mail para resetear el password a ${emailDestination}`
      );
    });

    cb(null);
  });
};

usuarioSchema.methods.reservar = function (bicicleta, desde, hasta, cb) {
  var reserva = new Reserva({
    usuario: this._id,
    bicicleta,
    desde,
    hasta
  });
  console.log(reserva);
  reserva.save(cb);
};

usuarioSchema.methods.enviar_mail_bienvenida = function (cb) {
  const token = new Token({
    _userId: this.id,
    token: crypto.randomBytes(16).toString("hex")
  });
  const emailDestination = this.email;

  token.save(function (err) {
    if (err) return console.log(err.message);

    const mailOptions = {
      from: "no-reply@redbicicletas.com",
      to: emailDestination,
      subject: "Verificacion de Cuenta",
      text:
        "Hola,?\n\n" +
        "Por favor, para verificar su cuenta haga click en este link: \n" +
        "http://localhost:3000" +
        "/token/confirmation/" +
        token.token +
        ".\n"
    };

    mailer.sendMail(mailOptions, function (err) {
      if (err) return console.log(err.message);

      console.log("Verification sent to: " + emailDestination);
    });
  });
};

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(
  condition,
  callback
) {
  const self = this;

  const googleId = condition.id;
  const email = condition.emails[0].value;

  self.findOne({ $or: [{ googleId }, { email }] }, (err, result) => {
    if (result) callback(err, result);
    else {
      const values = {
        googleId,
        email,
        nombre: condition.displayName || "SIN NOMBRE",
        verificado: true,
        password: crypto.randomBytes(16).toString("hex")
      };

      console.log("-----------------VALUES-----------------", {
        condition,
        values
      });

      self.create(values, (err, result) => {
        if (err) console.log(err);
        return callback(err, result);
      });
    }
  });
};

usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(
  condition,
  callback
) {
  const self = this;

  const facebookId = condition.id;
  const email = condition.emails[0].value || `${facebookId}@asd.asd`;

  self.findOne({ $or: [{ facebookId }, { email }] }, (err, result) => {
    if (result) callback(err, result);
    else {
      const values = {
        facebookId,
        email,
        nombre: condition.displayName || "SIN NOMBRE",
        verificado: true,
        password: crypto.randomBytes(16).toString("hex")
      };

      console.log("-----------------VALUES-----------------", {
        condition,
        values
      });

      self.create(values, (err, result) => {
        if (err) console.log(err);
        return callback(err, result);
      });
    }
  });
};

module.exports = mongoose.model("Usuario", usuarioSchema);
