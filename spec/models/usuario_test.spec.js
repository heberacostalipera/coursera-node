var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var Usuario = require('../../models/usuario');
var Reserva = require('../../models/reserva');
const usuario = require('../../models/usuario');

describe("Testing Usuarios", function () {
  beforeAll(done => {
    var mongoDB = 'mongodb://localhost/testdb';
    mongoose.connect(mongoDB, { useNewUrlParser: true })
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'MongoDB Connection Error: '));
    db.once('open', () => {
      console.log("Connected to test db");
      done();
    })
  });

  afterEach(done => {
    Reserva.deleteMany({}, function (err, success) {
      if (err) console.log(err)
      Usuario.deleteMany({}, function () {
        if (err) console.log(err)
        Bicicleta.deleteMany({}, (err, success) => {
          if (err) console.log(err);
          done();
        })
      })
    })
  });

  describe("Cuando un usuario reserva una bici", () => {
    it("debe existir la reserva", done => {
      const user = new Usuario({ nombre: "Juan" });
      user.save();
      const bici = new Bicicleta({ code: 1, color: "rojo", modelo: "urbana" });
      bici.save();

      var hoy = new Date();
      var mañana = new Date();

      mañana.setDate(hoy.getDate() + 1);

      user.reservar(bici.id, hoy, mañana, function (err, reserva) {
        Reserva.find({}).populate('bicicleta').populate('usuario').exec(function (err, reservas) {
          console.log(reservas[0]);
          expect(reservas.length).toBe(1);
          expect(reservas[0].diasDeReserva()).toBe(2);
          expect(reservas[0].bicicleta.code).toBe(1);
          expect(reservas[0].usuario.nombre).toBe(user.nombre);
          done();
        });
      });
    });
  });
});

