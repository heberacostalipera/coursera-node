var Bicicleta = require("../../models/bicicleta");

exports.bicicleta_list = function (req, res) {
  Bicicleta.allBicis(function (err, bicicletas) {
    res.status(200).json({ bicicletas });
  });
}

exports.bicicleta_create = function (req, res) {
  const { id, color, modelo, latitud, longitud } = req.body
  var bicicleta = new Bicicleta({ code: id, color, modelo, ubicacion: [latitud, longitud] })
  Bicicleta.add(bicicleta, function (err) {
    res.status(200).json({
      bicicleta
    });
  });
}

// exports.bicicleta_update = function (req, res) {
//   var bici = Bicicleta.findById(req.params.id)
//   const { id, color, modelo, latitud, longitud } = req.body

//   bici.id = id;
//   bici.color = color;
//   bici.modelo = modelo;
//   bici.ubicacion = [latitud, longitud];

//   res.status(200).json({
//     bicicleta: bici
//   })
// };

exports.bicicleta_delete = function (req, res) {
  Bicicleta.deleteByCode(req.body.id, function (err) {
    res.status(204).send();
  });
};