var Usuario = require("../../models/usuario");

exports.usuarios_list = function (req, res) {
  Usuario.find({}, function (err, usuarios) {
    res.status(200).json({
      usuarios
    });
  });
};

exports.usuarios_create = function (req, res) {
  var newUser = new Usuario({
    nombre: req.body.nombre,
    email: req.body.email,
    password: req.body.password
  });

  newUser.save(function (err) {
    if (err) return res.status(500).json(err);
    res.status(200).json(newUser);
  });
};

exports.usuario_reservar = function (req, res) {
  const { id, biciId, desde, hasta } = req.body;
  Usuario.findById(id, function (err, user) {
    console.log(user);
    user.reservar(biciId, desde, hasta, function (err) {
      console.log("reserva!");
      res.status(200).send();
    });
  });
};
