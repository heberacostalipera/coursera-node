const Usuario = require("../../models/usuario");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const notFound = {
  status: "error",
  message: "Email / password invalido",
  data: null
};

module.exports = {
  authenticate: function (req, res, next) {
    Usuario.findOne({ email: req.body.email }, function (err, usuario) {
      if (err) next(err);
      else {
        if (usuario === null) {
          return res.status(401).json(notFound);
        }

        if (
          usuario != null &&
          bcrypt.compareSync(req.body.password, usuario.password)
        ) {
          const token = jwt.sign(
            { id: usuario._id },
            req.app.get("secretKey"),
            { expiresIn: "7d" }
          );

          res.status(200).json({
            message: "usuario encontrado",
            data: {
              usuario,
              token
            }
          });
        } else {
          res.status(401).json(notFound);
        }
      }
    });
  },
  forgotPassword: function (req, res, next) {
    Usuario.findOne({ email: req.body.email }, function (err, usuario) {
      if (!usuario) {
        return res.status(401).json({
          message: "no existe usuario",
          data: null
        });
      }

      usuario.resetPassword(function (err) {
        if (err) return next(err);
        res.status(200).json({
          message: "Se envio un mail para reestablecer la contraseña",
          data: null
        });
      });
    });
  },
  authFacebookToken: function (req, res, next) {
    if (req.user) {
      req.user
        .save()
        .then(() => {
          const token = jwt.sign(
            { id: req.user.id },
            req.app.get("secretKey"),
            { expiresIn: "7d" }
          );
          res.status(200).json({
            message: "usuario encontrado o creado",
            data: { user: req.user, token }
          });
        })
        .catch(err => {
          console.log(err);
          res.status(500).json({ message: err.message });
        });
    } else res.status(401);
  }
};
