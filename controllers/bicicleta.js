var Bicicleta = require("../models/bicicleta");

exports.bicicleta_list = function (req, res) {
  Bicicleta.allBicis((err, bicis) => {
    if (err) res.redirect("/");
    res.render("bicicletas/index", {
      bicis
    });
  });
};

exports.bicicleta_create_get = function (req, res) {
  res.render("bicicletas/create");
};

exports.bicicleta_create_post = function (req, res) {
  const { id, color, modelo, latitud, longitud } = req.body;
  Bicicleta.create({
    id,
    color,
    modelo,
    ubicacion: [latitud, longitud]
  });

  res.redirect("/bicicletas");
};

exports.bicicleta_delete_post = function (req, res) {
  Bicicleta.removeById(req.body.id);
  res.redirect("/bicicletas");
};

exports.bicicleta_update_get = function (req, res) {
  var bici = Bicicleta.findById(req.params.id);
  res.render("bicicletas/update", { bici });
};

exports.bicicleta_update_post = function (req, res) {
  var bici = Bicicleta.findById(req.params.id);
  const { id, color, modelo, latitud, longitud } = req.body;
  bici.id = id;
  bici.color = color;
  bici.modelo = modelo;
  bici.ubicacion = [latitud, longitud];

  res.redirect("/bicicletas");
};
