const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const Usuario = require("../models/usuario");
var GoogleStrategy = require("passport-google-oauth20").Strategy;
var FacebookTokenStrategy = require("passport-facebook-token");

passport.use(
  new FacebookTokenStrategy(
    {
      clientID: process.env.FACEBOOK_APP_ID,
      clientSecret: process.env.FACEBOOK_APP_SECRET,
      fbGraphVersion: "v3.0",
      profileFields: ["id", "emails", "name"]
    },
    function (accessToken, refreshToken, profile, done) {
      Usuario.findOneOrCreateByFacebook(profile, function (error, user) {
        return done(error, user);
      });
    }
  )
);

passport.use(
  new LocalStrategy(function (email, password, done) {
    Usuario.findOne({ email }, function (err, usuario) {
      if (err) return done(err);
      if (!usuario)
        return done(null, false, {
          message: "Email no existente o incorrecto"
        });
      if (!usuario.validPassword(password))
        return done(null, false, {
          message: "Password incorrecto"
        });

      return done(null, usuario);
    });
  })
);

passport.use(
  new GoogleStrategy(
    {
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      callbackURL: `${process.env.HOST}/auth/google/callback`
    },
    function (request, accessToken, refreshToken, profile, done) {
      Usuario.findOneOrCreateByGoogle(profile, function (err, user) {
        return done(err, user);
      });
    }
  )
);

passport.serializeUser(function (user, cb) {
  cb(null, user.id);
});

passport.deserializeUser(function (id, cb) {
  Usuario.findById(id, function (err, usuario) {
    cb(err, usuario);
  });
});

module.exports = passport;
