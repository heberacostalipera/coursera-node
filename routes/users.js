var express = require('express');
var router = express.Router();
var usuariosController = require("../controllers/usuarios")

router.get("/", usuariosController.list);
// Create
router.get("/create", usuariosController.create_get);
router.post("/create", usuariosController.create);
// Update
router.get("/:id/update", usuariosController.update_get);
router.post("/:id/update", usuariosController.update);
// Delete
router.post("/:id/delete", usuariosController.delete);

module.exports = router;
